﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using API2SoftPlan.Entities;
using API2SoftPlan.Services;
using Microsoft.AspNetCore.Mvc;

namespace API2SoftPlan.Controllers
{
    [Route("api/v1/account")]
    public class LoginController : Controller
    {

        /// <summary>
        /// Seleciona token para autorização em outra chamadas.
        /// </summary>
        /// <remarks>
        /// Exemplo de uso:
        ///  
        ///     POST /api/v1/account/login/{username}/{password}
        ///     {
        ///        "username":"Leonardo",
        ///        "password":"Piccoli",
        ///     }
        /// 
        /// </remarks>
        /// <param name="username">Username do usuário</param>
        /// <param name="password">Senha do usuário</param>
        /// <returns>Token do usuário</returns>
        // POST api/v1/account/login
        [HttpPost]
        [Route("login/{username}/{password}")]
        public async Task<ActionResult<dynamic>> Authenticate(string username, string password)
        {

            try
            {

                // Recupera o usuário
                UserEntities userEnt = new UserEntities();
                userEnt.login = username;
                userEnt.pass = password;

                

                // Verifica se o usuário existe
                if (userEnt.login != "Leonardo" || userEnt.pass != "Piccoli")
                    return NotFound(new { message = "Usuário ou senha inválidos" });

                // Gera o Token
                var token = TokenService.GenerateToken(userEnt);

                // Retorna os dados
                return new
                {
                    user = userEnt,
                    token = token
                };
            }
            catch (Exception ex)
            {

                return new
                {
                    StatusCode = Microsoft.AspNetCore.Http.StatusCodes.Status500InternalServerError,
                    message = "Foram encontrados problemas ao autenticar usuário"
                };
            }
        }
    }
}