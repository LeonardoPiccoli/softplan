﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace API2SoftPlan.Controllers
{
    [Route("api/v1/showmethecode")]
    [ApiController]
    public class CodeController : ControllerBase
    {
        // GET api/values
        [HttpGet]
        [Authorize]
        public ActionResult<IEnumerable<string>> Get()
        {
            return new string[] { "https://LeonardoPiccoli@bitbucket.org/LeonardoPiccoli/softplan.git" };
        }
    }
}
