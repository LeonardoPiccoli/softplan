﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace API2SoftPlan.Controllers
{
    [Route("api/v1/[controller]")]
    public class CalculaJurosController : Controller
    {

        /// <summary>
        /// Lista as informações do processo especificado.
        /// </summary>
        /// <remarks>
        /// Exemplo de uso:
        ///  
        ///     GET api/v1/calculajuros"
        /// 
        /// </remarks>
        /// <returns>Dados do processo especificado</returns>
        // POST api/v1/calculajuros
        [HttpGet]
        [Route("{valor:decimal}/{tempo:int}")]
        [Authorize]
        public async Task<IActionResult> Get(decimal valor, int tempo)
        {
        
            decimal ValorFinal = 0.00M;
            double TaxaJuros = 0;
            using (var client = new HttpClient())
            {

                HttpResponseMessage response = client.GetAsync(string.Format("{0}taxaJuros",
                Settings.TaxaJuros)).Result;
                
                string apiResponse = await response.Content.ReadAsStringAsync();
                TaxaJuros = Convert.ToDouble(apiResponse);
            }
            double x = Convert.ToDouble(valor);

            double y = Convert.ToDouble(tempo);
            ValorFinal = Math.Round(Convert.ToDecimal(x * Math.Pow((1 + TaxaJuros), y)),2,0);
  
            return Ok(ValorFinal.ToString());
        }

        
    }
}
