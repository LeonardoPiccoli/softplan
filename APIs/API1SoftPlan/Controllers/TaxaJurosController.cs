﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace API1SoftPlan.Controllers
{
    [Route("api/v1/[controller]")]
    public class TaxaJurosController : Controller
    {

        /// <summary>
        /// Lista as informações do processo especificado.
        /// </summary>
        /// <remarks>
        /// Exemplo de uso:
        ///  
        ///     GET api/v1/taxaJuros
        /// 
        /// </remarks>
        /// <returns>Dados do processo especificado</returns>
        // GET api/v1/taxaJuros
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            return Ok("0,01");
        }

        
    }
}
